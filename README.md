# AsToRia: Thermodynamic Properties

This C library loads the thermodynamic properties of a gas mixture.
See `atrtp.5` for format specification.

## Requirements

- C compiler
- POSIX make
- pkg-config
- [RSys](https://gitlab.com/vaplv/rsys)
- [mandoc](https://mandoc.bsd.lv)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

###  Version 0.1

- Write the man page directly in mdoc's roff macros, instead of using
  the scdoc markup language as a source for man pages.
- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

### Version 0.0.1

Use scdoc rather than asciidoc as file format for man sources.

## Copyright

Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)  
Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique (CNRS)

## License

AtrTP is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
