.\" Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
.\" Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
.\"
.\" This program is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program. If not, see <http://www.gnu.org/licenses/>.
.Dd August 30, 2023
.Dt ATRTP 5
.Os
.Sh NAME
.Nm atrtp
.Nd AsToRia: Thermodynamic Properties
.Sh DESCRIPTION
.Nm
is a binary file format to store a set of thermodynamic porperties for each
node of a volumetric mesh representing a medium in combustion.
An
.Nm
file begins by a header that describes the data layout followed by a list of
per node properties.
.Pp
The header consist of 2 integers.
The first integer is a power of two
.Pq usually 4096
that defines the size of the memory page in bytes
.Pq Va pagesize
on which the thermodynamic properties are aligned.
By aligning data to
.Va pagesize ,
and depending on system requirements, memory mapping can be used to
automatically load/unload pages on demand
.Pq see Xr mmap 2 .
The other integer stores the number of elements in the list.
In the following, we'll call it
.Va #nodes ,
for "number of nodes", in reference to the mesh nodes to which these properties
will be attached.
.Pp
Fill bytes follow the file header to align thermodynamic
properties to
.Va pagesize .
Properties are then listed with 8 double-precision floating-point numbers per
element (i.e. per node).
The data per node are as follows:
.Bl -dash -offset indent -compact
.It
The pressure in Pascal
.It
The temperature in Kelvin
.It
The molar fraction of H2O in mol(H2O)/mol(mixture)
.It
The molar fraction of CO2 in mol(CO2)/mol(mixture)
.It
The molar fraction of CO in mol(CO)/mol(mixture)
.It
The volumic fraction of soot in m^3(soot)/m^3
.It
The number of soot primary particules per agregate
.It
The soot primary particles diameter in nm
.El
.Pp
Finally, fill bytes are added to align the overall file size to
.Va pagesize .
.Pp
Data are encoded with respect to the little endian bytes ordering, i.e. least
significant bytes are stored first.
.Pp
The file format is as follows:
.Bl -column (thermo-props) (::=) ()
.It Ao Va atrtp Ac Ta ::= Ta Ao Va pagesize Ac Ao Va #nodes Ac
.It Ta Ta Aq Va padding
.It Ta Ta Aq Va thermo-props
.It Ta Ta Aq Va padding
.It Ao Va pagesize Ac Ta ::= Ta Vt uint64_t
.It Ao Va #nodes Ac Ta ::= Ta Vt uint64_t
.It \  Ta Ta
.It Ao Va padding Ac Ta ::= Ta Op Vt int8_t ...
# Ensure alignment on
.Va pagesize
.It \  Ta Ta
.It Ao Va thermo-props Ac Ta ::= Ta Ao Va thermo-prop Ac Va ...
.It Ao Va thermo-prop Ac Ta ::= Ta Ao Va pressure Ac
# In Pascal
.It Ta Ta Ao Va temperature Ac
.It Ta Ta Ao Va xH2O Ac # Molar fraction of H2O
.It Ta Ta Ao Va xCO2 Ac # Molar fraction of CO2
.It Ta Ta Ao Va xCO Ac # Molar fraction of CO
.It Ta Ta Ao Va vf-soot Ac # Volumic fraction of soot
.It Ta Ta Ao Va np-soot Ac # Soot particles per agregate
.It Ta Ta Ao Va dp-soot Ac # Soot particles diameter
.It \  Ta Ta
.It Ao Va pressure Ac Ta ::= Ta Vt double
# In Pascal
.It Ao Va temperature Ac Ta ::= Ta Vt double
# In Kelvin
.It Ao Va xH2O Ac Ta ::= Ta Vt double
# In mol(H2O)/mol(mixture)
.It Ao Va xCO2 Ac Ta ::= Ta Vt double
# In mol(CO2)/mol(mixture)
.It Ao Va xCO Ac Ta ::= Ta Vt double
# In mol(CO)/mol(mixture)
.It Ao Va vf-soot Ac Ta ::= Ta Vt double
# In m^3(soot)/m^3
.It Ao Va np-soot Ac Ta ::= Ta Vt double
.It Ao Va dp-soot Ac Ta ::= Ta Vt double
# In nm
.El
.Sh SEE ALSO
.Xr mmap 2
