/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRTP_H
#define ATRTP_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(ATRTP_SHARED_BUILD) /* Build shared library */
  #define ATRTP_API extern EXPORT_SYM
#elif defined(ATRTP_STATIC) /* Use/build static library */
  #define ATRTP_API extern LOCAL_SYM
#else /* Use shared library */
  #define ATRTP_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the atrtp function `Func'
 * returns an error. One should use this macro on sth function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define ATRTP(Func) ASSERT(atrtp_ ## Func == RES_OK)
#else
  #define ATRTP(Func) atrtp_ ## Func
#endif

/* Type of the thermodynamic properties */
enum atrtp_type {
  ATRTP_PRESSURE, /* In Pascal */
  ATRTP_TEMPERATURE, /* In Kelvin */
  ATRTP_XH20, /* In mol(H2O)/mol(mixture) */
  ATRTP_XCO2, /* In mol(CO2)/mol(mixture) */
  ATRTP_XCO, /* In mol(CO)/mol(mixture) */
  ATRTP_SOOT_VOLFRAC, /* In m^3(soot)/m^3 */
  ATRTP_SOOT_PRIMARY_PARTICLES_COUNT, /* #primary particles per aggregate */
  ATRTP_SOOT_PRIMARY_PARTICLES_DIAMETER, /* Primary particles diameter in nm */
  ATRTP_COUNT__
};

struct atrtp_desc {
  const double* properties; /* List of double[ATRTP_COUNT__] */
  size_t nnodes;
};
static const struct atrtp_desc ATRTP_DESC_NULL;

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

/* Forward declaration of opaque data types */
struct atrtp;

BEGIN_DECLS

/*******************************************************************************
 * ATRTP API
 ******************************************************************************/
ATRTP_API res_T
atrtp_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   const int verbose, /* Verbosity level */
   struct atrtp** atrtp);

ATRTP_API res_T
atrtp_ref_get
  (struct atrtp* atrtp);

ATRTP_API res_T
atrtp_ref_put
  (struct atrtp* attrtp);

ATRTP_API res_T
atrtp_load
  (struct atrtp* atrtp,
   const char* path);

ATRTP_API res_T
atrtp_load_stream
  (struct atrtp* atrtp,
   FILE* stream,
   const char* stream_name); /* Can be NULL */

ATRTP_API res_T
atrtp_get_desc
  (const struct atrtp* atrtp,
   struct atrtp_desc* desc);

static INLINE const double*
atrtp_desc_get_node_properties
  (const struct atrtp_desc* desc,
   const size_t inode)
{
  ASSERT(desc && inode < desc->nnodes);
  return desc->properties + inode*ATRTP_COUNT__;
}

END_DECLS

#endif /* ATRTP_H */
