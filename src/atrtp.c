/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* mmap support */
#define _DEFAULT_SOURCE 1 /* MAP_POPULATE support */
#define _BSD_SOURCE 1 /* MAP_POPULATE for glibc < 2.19 */

#include "atrtp.h"
#include "atrtp_c.h"
#include "atrtp_log.h"

#include <unistd.h>

#include <errno.h>
#include <sys/mman.h> /* mmap */

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
reset_atrtp(struct atrtp* atrtp)
{
  ASSERT(atrtp);
  atrtp->pagesize = 0;
  atrtp->nnodes = 0;
  if(atrtp->props) munmap(atrtp->props, atrtp->map_len);
  atrtp->props = NULL;
  atrtp->map_len = 0;
}

static res_T
load_stream(struct atrtp* atrtp, FILE* stream, const char* stream_name)
{
  off_t offset;
  size_t filesz;
  res_T res = RES_OK;
  ASSERT(atrtp && stream && stream_name);

  reset_atrtp(atrtp);

  /* Read file header */
  #define READ(Var, N, Name) {                                                 \
    if(fread((Var), sizeof(*(Var)), (N), stream) != (N)) {                     \
      log_err(atrtp, "%s: could not read the %s.\n", stream_name, (Name));     \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&atrtp->pagesize, 1, "page size");
  READ(&atrtp->nnodes, 1, "number of nodes");
  #undef READ

  if(!IS_ALIGNED(atrtp->pagesize, atrtp->pagesize_os)) {
    log_err(atrtp,
      "%s: invalid page size %li. The page size attribute must be aligned on "
      "the page size of the operating system (%lu).\n",
      stream_name, atrtp->pagesize, (unsigned long)atrtp->pagesize_os);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Compute the length in bytes of the data to map */
  atrtp->map_len = atrtp->nnodes * sizeof(double[ATRTP_COUNT__]);
  atrtp->map_len = ALIGN_SIZE(atrtp->map_len, (size_t)atrtp->pagesize);

  /* Find the offsets of the positions/indices data into the stream */
  offset = (off_t)ALIGN_SIZE((uint64_t)ftell(stream), atrtp->pagesize);

  /* Retrieve the overall filesize */
  fseek(stream, 0, SEEK_END);
  filesz = (size_t)ftell(stream);

  /* Check that the file has sufficient data */
  if((size_t)offset + atrtp->map_len > filesz) {
    log_err(atrtp,
      "%s: the thermodynamic properties to load exceed the file size.\n",
      stream_name);
    res = RES_IO_ERR;
    goto error;
  }

  /* Map the thermodynamic properties */
  atrtp->props = mmap(NULL, atrtp->map_len, PROT_READ,
    MAP_PRIVATE|MAP_POPULATE, fileno(stream), offset);
  if(atrtp->props == MAP_FAILED) {
    log_err(atrtp, "%s: could not map the thermodynamic properties --%s.\n",
      stream_name, strerror(errno));
    res = RES_IO_ERR;
    goto error;
  }

exit:
  return res;
error:
  reset_atrtp(atrtp);
  goto exit;
}

static void
release_atrtp(ref_T* ref)
{
  struct atrtp* atrtp;
  ASSERT(ref);
  atrtp = CONTAINER_OF(ref, struct atrtp, ref);
  reset_atrtp(atrtp);
  if(atrtp->logger == &atrtp->logger__) logger_release(&atrtp->logger__);
  MEM_RM(atrtp->allocator, atrtp);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
atrtp_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* mem_allocator, /* NULL <=> use default allocator */
   const int verbose, /* Verbosity level */
   struct atrtp** out_atrtp)
{
  struct atrtp* atrtp = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;

  if(!out_atrtp) {
    res = RES_BAD_ARG;
    goto error;
  }

  allocator = mem_allocator ? mem_allocator : &mem_default_allocator;
  atrtp = MEM_CALLOC(allocator, 1, sizeof(*atrtp));
  if(!atrtp) {
    if(verbose) {
      #define ERR_STR "Could not allocate the AtrTP device.\n"
      if(logger) {
        logger_print(logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&atrtp->ref);
  atrtp->allocator = allocator;
  atrtp->verbose = verbose;
  atrtp->pagesize_os = (size_t)sysconf(_SC_PAGESIZE);
  if(logger) {
    atrtp->logger = logger;
  } else {
    setup_log_default(atrtp);
  }

exit:
  if(out_atrtp) *out_atrtp = atrtp;
  return res;
error:
  if(atrtp) {
    ATRTP(ref_put(atrtp));
    atrtp = NULL;
  }
  goto exit;
}

res_T
atrtp_ref_get(struct atrtp* atrtp)
{
  if(!atrtp) return RES_BAD_ARG;
  ref_get(&atrtp->ref);
  return RES_OK;
}

res_T
atrtp_ref_put(struct atrtp* atrtp)
{
  if(!atrtp) return RES_BAD_ARG;
  ref_put(&atrtp->ref, release_atrtp);
  return RES_OK;
}

res_T
atrtp_load(struct atrtp* atrtp, const char* path)
{
  FILE* file = NULL;
  res_T res = RES_OK;

  if(!atrtp || !path) {
    res = RES_BAD_ARG;
    goto error;
  }

  file = fopen(path, "r");
  if(!file) {
    log_err(atrtp, "%s: error opening file `%s'.\n", FUNC_NAME, path);
    res = RES_IO_ERR;
    goto error;
  }

  res = load_stream(atrtp, file, path);
  if(res != RES_OK) goto error;

exit:
  if(file) fclose(file);
  return res;
error:
  goto exit;
}

res_T
atrtp_load_stream
  (struct atrtp* atrtp,
   FILE* stream,
   const char* stream_name)
{
  if(!atrtp || !stream) return RES_BAD_ARG;
  return load_stream(atrtp, stream, stream_name ? stream_name : "<stream>");
}

res_T
atrtp_get_desc(const struct atrtp* atrtp, struct atrtp_desc* desc)
{
  if(!atrtp || !desc) return RES_BAD_ARG;
  desc->properties = atrtp->props;
  desc->nnodes = (size_t)atrtp->nnodes;
  return RES_OK;
}

