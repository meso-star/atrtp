/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "atrtp.h"
#include <rsys/mem_allocator.h>
#include <stdio.h>
#include <unistd.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
check_atrtp_desc
  (const struct atrtp_desc* desc,
   const uint64_t nnodes)
{
  size_t inode;
  CHK(desc);
  CHK(nnodes);

  CHK(desc->nnodes == nnodes);
  FOR_EACH(inode, 0, nnodes) {
    size_t iprop;
    const double* props = atrtp_desc_get_node_properties(desc, inode);
    CHK(props);
    FOR_EACH(iprop, 0, ATRTP_COUNT__) {
      CHK(props[iprop] == (double)(10+iprop));
    }
  }
}

static void
test_load(struct atrtp* atrtp)
{
  struct atrtp_desc desc = ATRTP_DESC_NULL;
  FILE* fp = NULL;
  const char* filename = "test_file.atrtp";
  const uint64_t pagesize = 16384;
  const uint64_t nnodes = 192;
  size_t i;
  char byte = 0;

  fp = fopen(filename, "w+");
  CHK(fp);

  /* Write the header */
  CHK(fwrite(&pagesize, sizeof(pagesize), 1, fp) == 1);
  CHK(fwrite(&nnodes, sizeof(nnodes), 1, fp) == 1);

  /* Padding */
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET) == 0);

  /* Write nodes data */
  FOR_EACH(i, 0, nnodes) {
    double props[ATRTP_COUNT__];
    int iprop;
    CHK(ATRTP_COUNT__ < 10);
    FOR_EACH(iprop, 0, ATRTP_COUNT__) {
      props[iprop] = (double)(10 + iprop);
    }
    CHK(fwrite(props, sizeof(double), ATRTP_COUNT__, fp) == ATRTP_COUNT__);
  }

  /* Padding. Write one char to position the EOF indicator */
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize)-1, SEEK_SET) == 0);
  CHK(fwrite(&byte, sizeof(byte), 1, fp) == 1);

  rewind(fp);

  CHK(atrtp_load_stream(NULL, fp, filename) == RES_BAD_ARG);
  CHK(atrtp_load_stream(atrtp, NULL, filename) == RES_BAD_ARG);
  CHK(atrtp_load_stream(atrtp, fp, NULL) == RES_OK);
  CHK(atrtp_get_desc(NULL, &desc) == RES_BAD_ARG);
  CHK(atrtp_get_desc(atrtp, NULL) == RES_BAD_ARG);
  CHK(atrtp_get_desc(atrtp, &desc) == RES_OK);

  rewind(fp);
  CHK(atrtp_load_stream(atrtp, fp, filename) == RES_OK);
  CHK(atrtp_get_desc(atrtp, &desc) == RES_OK);
  check_atrtp_desc(&desc, nnodes);

  CHK(atrtp_load(NULL, filename) == RES_BAD_ARG);
  CHK(atrtp_load(atrtp, NULL) == RES_BAD_ARG);
  CHK(atrtp_load(atrtp, "nop") == RES_IO_ERR);
  CHK(atrtp_load(atrtp, filename) == RES_OK);
  CHK(atrtp_get_desc(atrtp, &desc) == RES_OK);
  check_atrtp_desc(&desc, nnodes);

  fclose(fp);
}

static void
test_load_fail(struct atrtp* atrtp)
{
  const char byte = 1;
  FILE* fp = NULL;
  uint64_t pagesize;
  uint64_t nnodes;

  CHK(atrtp);

  /* Wrong pagesize */
  fp = tmpfile();
  CHK(fp);
  pagesize = 4097;
  nnodes = 10;
  CHK(fwrite(&pagesize, sizeof(pagesize), 1, fp) == 1);
  CHK(fwrite(&nnodes, sizeof(nnodes), 1, fp) == 1);
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET) == 0);
  CHK(fseek(fp, (long)(sizeof(double[ATRTP_COUNT__])*nnodes), SEEK_CUR) == 0);
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize)-1, SEEK_SET) == 0);
  CHK(fwrite(&byte, sizeof(byte), 1, fp) == 1); /* Positioned the EOF */
  rewind(fp);
  CHK(atrtp_load_stream(atrtp, fp, NULL) == RES_BAD_ARG);
  fclose(fp);

  /* Wrong size */
  fp = tmpfile();
  CHK(fp);
  pagesize = (uint64_t)sysconf(_SC_PAGESIZE);
  nnodes = 10;
  CHK(fwrite(&pagesize, sizeof(pagesize), 1, fp) == 1);
  CHK(fwrite(&nnodes, sizeof(nnodes), 1, fp) == 1);
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET) == 0);
  CHK(fseek(fp, (long)(sizeof(double[ATRTP_COUNT__])*nnodes), SEEK_CUR) == 0);
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize)-2, SEEK_SET) == 0);
  CHK(fwrite(&byte, sizeof(byte), 1, fp) == 1); /* Positioned the EOF */

  rewind(fp);
  CHK(atrtp_load_stream(atrtp, fp, NULL) == RES_IO_ERR);
  fclose(fp);
}

static void
test_load_files(struct atrtp* atrtp, int argc, char** argv)
{
  int i;
  CHK(atrtp);
  FOR_EACH(i, 1, argc) {
    struct atrtp_desc desc = ATRTP_DESC_NULL;
    size_t icell;

    printf("Load %s\n", argv[i]);
    CHK(atrtp_load(atrtp, argv[i]) == RES_OK);
    CHK(atrtp_get_desc(atrtp, &desc) == RES_OK);

    FOR_EACH(icell, 0, desc.nnodes) {
      const double* props = atrtp_desc_get_node_properties(&desc, icell);
      size_t iprop;
      FOR_EACH(iprop, 0, ATRTP_COUNT__) {
        CHK(props[iprop] == props[iprop]); /* !NaN */
      }
      CHK(props[ATRTP_PRESSURE] >= 0);
      CHK(props[ATRTP_TEMPERATURE] >= 0);
      CHK(props[ATRTP_XH20] >= 0);
      CHK(props[ATRTP_XCO2] >= 0);
      CHK(props[ATRTP_XCO] >= 0);
      CHK(props[ATRTP_SOOT_VOLFRAC] >= 0);
      CHK(props[ATRTP_SOOT_VOLFRAC] <= 1);
      CHK(props[ATRTP_SOOT_PRIMARY_PARTICLES_COUNT] >= 0);
      CHK(props[ATRTP_SOOT_PRIMARY_PARTICLES_DIAMETER] >= 0);
    }
  }
}

/*******************************************************************************
 * Main function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct atrtp* atrtp = NULL;
  (void)argc, (void)argv;

  CHK(atrtp_create(NULL, &mem_default_allocator, 1, &atrtp) == RES_OK);

  test_load(atrtp);
  test_load_fail(atrtp);
  test_load_files(atrtp, argc, argv);

  CHK(atrtp_ref_put(atrtp) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
