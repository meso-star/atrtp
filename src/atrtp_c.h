/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRTP_C_H
#define ATRTP_C_H

#include <rsys/logger.h>
#include <rsys/ref_count.h>

struct mem_allocator;

struct atrtp {
  uint64_t pagesize;
  uint64_t nnodes;

  double* props;
  size_t map_len;

  size_t pagesize_os;

  struct mem_allocator* allocator;
  struct logger* logger;
  struct logger logger__; /* Default logger */
  int verbose;
  ref_T ref;
};

#endif /* ATRTP_C_H */
